import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class SomeRandomFunctionsKtTest {

    @Test
    fun generateArrayInRange() {
        assertAll(
            {assertEquals(listOf(7,8,9,10,11,12,13,14,15), generateArrayInRange(7,15))},
            {assertEquals(listOf(5,4,3,2,1,0,-1,-2,-3,-4,-5), generateArrayInRange(5,-5))})
    }

    @Test
    fun minValue() {
        assertAll(
            {assertEquals(10, minValue(listOf(10,33,44,234,545,312,15,31231,65,34534,54)))},
            {assertEquals(-999, minValue(listOf(-999,33,44,234,545,312,15,31231,65,34534,54)))})

    }

    @Test
    fun selectGrade() {
        assertAll(
            { assertEquals("No presentat",selectGrade(0))},
            { assertEquals("Insuficient",selectGrade(1))},
            { assertEquals("Insuficient",selectGrade(2))},
            { assertEquals("Insuficient",selectGrade(3))},
            { assertEquals("Insuficient",selectGrade(4))},
            { assertEquals("Suficient",selectGrade(5))},
            { assertEquals("Suficient",selectGrade(6))},
            { assertEquals("Notable",selectGrade(7))},
            { assertEquals("Notable",selectGrade(8))},
            { assertEquals("Excel·lent",selectGrade(9))},
            { assertEquals("Excel·lent + MH",selectGrade(10))},
            { assertEquals("No valid",selectGrade(11))},
            { assertEquals("No valid",selectGrade(-500))}
        )
    }

    @Test
    fun charPositionsInString() {
        val inputTest = "hola que tal"
        assertAll(
            { assertEquals(listOf(3,10), charPositionsInString(inputTest, 'a'))},
            { assertEquals(listOf<Int>(), charPositionsInString(inputTest, 'z'))})
    }

    @Test
    fun firstOccurrencePosition() {
        val inputTest = "hola que tal.";
        assertAll(
            { assertEquals(3, firstOccurrencePosition(inputTest, 'a'))},
            { assertEquals(0, firstOccurrencePosition(inputTest, 'h'))},
            { assertEquals(12, firstOccurrencePosition(inputTest, '.'))},
            { assertEquals(-1, firstOccurrencePosition(inputTest, 'z'))})
    }

    @Test
    fun roundGrade() {
        assertAll(
            { assertEquals(4,roundGrade(4.8))},
            { assertEquals(7,roundGrade(7.3))},
            { assertEquals(0,roundGrade(-4.4))},
            { assertEquals(9,roundGrade(8.7))})
    }
}