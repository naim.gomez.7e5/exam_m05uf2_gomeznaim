/**
 * Generate a list of numbers within the given range
 * @param firstValue (Int) The first number on the list
 * @param lastValue (Int) The last number on the list
 * @return A List<Int> of numbers from firstValue to lastValue
 */
fun generateArrayInRange(firstValue: Int, lastValue: Int): List<Int> {
    val listOfNumbers = mutableListOf<Int>()
    for (i in firstValue..lastValue) {
        listOfNumbers.add(i)
    }
    return listOfNumbers
}


/**
 *  Iterator over the given array to find the smallest value and return it
 *
 *  @param list (List<Int>) in which to search for the smallest number
 *  @return The smallest number on the list as Int
 */
fun minValue(list: List<Int>): Int {
    var minValue = 0
    for (i in list.indices) {
        if (i < minValue) {
            minValue = i
        } else if (i == minValue) {
            minValue = i
        }
    }
    return minValue
}


/**
 * Match the indicated index with a grade and
 * return the grade in String format
 *
 * @param index (Int) Identifier of the grade to select
 * @return A string with the selected grade
 */
fun selectGrade(index: Int): String {
    val selected : String = when (index) {
        0 -> "No presentat"
        1, 2, 3, 4 -> "Insuficient"
        5, 6 -> "Suficient"
        7 -> "Notable"
        10 -> "Excelent + MH"
        else -> "No valid"
    }
    return selected
}


/**
 * Search inside the string for a specific character and
 * return the indices where it is found.
 *
 * @param str (String) The string in which to find the char
 * @param letter (Char) The char to find
 * @return A List where each value is the index where is a char that match
 */
fun charPositionsInString(str: String, letter: Char): List<Int> {
    val matchedValues = mutableListOf<Int>()
    var coincidences = 0
    for (i in 0 until str.lastIndex) {
        if (str[i] == letter) {
            matchedValues.add(i)
        }
    }
    return matchedValues
}


/**
 * Search inside the string for a specific character and
 * return the index of the first char found.
 *
 * @param str (String) The string in which to find the char
 * @param letter (Char) The char to find
 * @return The index of the first matched char if not exits returns -1
 */
fun firstOccurrencePosition(str: String, letter: Char): Int {
    val pos: MutableList<Int> = mutableListOf()
    for (i in 0 until str.length) {
        if (str[i] == letter) {
            return i
        }
    }
    return -1
}


/**
 * Round the indicated grade, and return it as Int.
 *
 * @param grade (Int) number to round
 * @return The rounded grade as (Int)
 * @exception Exception If grade is <5 trunc grade
 */
fun roundGrade(grade: Double): Int {
    return grade.toInt()
}